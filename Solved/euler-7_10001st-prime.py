# 10001st prime
# Problem 7 
# By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, 
# we can see that the 6th prime is 13.

# What is the 10 001st prime number?


# Answer: 104743

import time

primes = [2]
count = 3
s = time.time()
def checkPrimality(arr, num):
    for i in range(1, len(arr)):
        if num%arr[i] == 0:
            return False
    return True

while len(primes) <= 10001:
    if checkPrimality(primes, count):
        primes.append(count)
    count += 2

print(primes[10000])
print(time.time() - s)