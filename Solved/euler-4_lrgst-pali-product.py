# Largest palindrome product
# Problem 4 
# A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

# Find the largest palindrome made from the product of two 3-digit numbers.


### Answer: 906609

def largestPalindromProduct():
    palindromeArr = []
    for i in range(100, 999):
        for j in range(100, 999):
            if (str(i*j) == str(i*j)[::-1]):
                palindromeArr.append(i*j)
    return max(palindromeArr)

print(largestPalindromProduct())

