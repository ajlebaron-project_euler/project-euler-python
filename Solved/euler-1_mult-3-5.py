#Multiples of 3 and 5
#Problem 1 
#If we list all the natural numbers below 10 that are multiples of 3 or 5, 
# we get 3, 5, 6 and 9. The sum of these multiples is 23.

#Find the sum of all the multiples of 3 or 5 below 1000.

### Answer 233168

def sumOfMultipesUnder1000(num1, num2):
    sum = 0
    num = num1 if num1 < num2 else num2
    for i in range(num, 1000):
        if (i%num1 == 0 or i%num2 == 0):
            sum = sum + i
    return sum

print(sumOfMultipesUnder1000(3, 5))