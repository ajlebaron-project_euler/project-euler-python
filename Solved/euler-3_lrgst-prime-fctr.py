# Largest prime factor
# Problem 3 
# The prime factors of 13195 are 5, 7, 13 and 29.

# What is the largest prime factor of the number 600851475143 ?


### Answer: 6857

def largestPrimeFactor(num):
    primes = []
    numTemp = num
    for i in range(2, num):
        if (numTemp%i==0):
            primes.append(i)
            numTemp = int(numTemp/i)
            if (numTemp == 1):
                return max(primes)
            largestPrimeFactor(numTemp)
    
print(largestPrimeFactor(600851475143))