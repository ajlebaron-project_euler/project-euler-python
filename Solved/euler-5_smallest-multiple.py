# Smallest multiple
# Problem 5 
# 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

# What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?


### Answer: 232792560

def smallestMultiple(num):

    if isinstance(num, int):
        arr = []
        arr.extend(range(1, num+1))
    else:
        print(len(num))
        arr = num
    lcm = []

    def gcd(a, b):
        d = 0
        while (a%2==0 and b%2==0):
            a = a/2
            b = b/2
            d = d + 1
        while (a!=b):
            if a%2==0:
                a = a/2
            elif b%2==0:
                b = b/2
            elif a>b:
                a = (a-b)/2
            else:
                b = (b-a)/2
        g = a
        return g*(2**d)

    for i in range(1, len(arr)):
        lcm.append((arr[i-1]*arr[i])/gcd(arr[i-1], arr[i]))
    
    if len(lcm)!=1:
        smallestMultiple(lcm)
    else: 
        print(int(lcm[0]))
    
    
# print(smallestMultiple10))
print(smallestMultiple(20))