# Summation of primes
# Problem 10 
# The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

# Find the sum of all the primes below two million.


# Answer: 142913828922

import time

s = time.time()

def primes_sieve(limit):
    fixIndexLimit = limit+1
    numbers = [False] * (fixIndexLimit)
    primes = []

    for i in range(2, fixIndexLimit):
        if numbers[i]:
            continue
        for j in range(i*2, fixIndexLimit, i):
            numbers[j] = True
        
        primes.append(i)

    return sum(i for i in primes)

print(primes_sieve(2000000))


print(time.time() - s)