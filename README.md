# Project Euler Challenges with Python

The [Project Euler](https://projecteuler.net/) is a website with challenges that involve the use both mathematical and numerical methods. The challenges in this repository are solved using Python.

**WARNING** These might not be (most likely won't be) the most efficient solutions. Just my solutions.


## Getting Started

Just get the script and run it in a terminal.

```bash
python euler-x_xxx.py
```

## Challenges Solved

1. Multiples of 3 and 5
1. Even Fibonacci numbers
1. Largest prime factor
1. Largest palindrome product
1. Smallest multiple
1. Sum square difference
1. 10001st Prime
1. Largest product in a series
1. Special Pythagorean triplet
1. Summation of primes 

## Built With

* [Project Euler](https://projecteuler.net/)
* [Python](https://www.python.org/)


## Authors

* [Alberto Jaime Le Baron](https://gitlab.com/anewmodern)